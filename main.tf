resource "aws_customer_gateway" "this" {
  bgp_asn    = var.bgp_asn
  ip_address = var.customer_gateway_ip_address
  type       = "ipsec.1"

  tags = merge(var.tags, local.tags, {
    Name = lower("vpn-cgw-${lower(var.name)}-tflz")
  })
}

resource "aws_vpn_connection" "this" {
  customer_gateway_id = aws_customer_gateway.this.id
  transit_gateway_id  = var.vpn_attachment_type == "tgw" ? var.transit_gateway_id : null
  type                = aws_customer_gateway.this.type

  static_routes_only = var.enable_static_routing

  tunnel1_preshared_key = var.tunnel1_preshared_key
  tunnel2_preshared_key = var.tunnel2_preshared_key

  tunnel1_phase1_dh_group_numbers = var.tunnel1_phase1_dh_group_numbers
  tunnel2_phase1_dh_group_numbers = var.tunnel2_phase1_dh_group_numbers

  tunnel1_phase1_encryption_algorithms = var.tunnel1_phase1_encryption_algorithms
  tunnel2_phase1_encryption_algorithms = var.tunnel2_phase1_encryption_algorithms

  tunnel1_phase1_integrity_algorithms = var.tunnel1_phase1_integrity_algorithms
  tunnel2_phase1_integrity_algorithms = var.tunnel2_phase1_integrity_algorithms

  tunnel1_phase1_lifetime_seconds = var.tunnel1_phase1_lifetime_seconds
  tunnel2_phase1_lifetime_seconds = var.tunnel2_phase1_lifetime_seconds

  tunnel1_dpd_timeout_seconds = var.tunnel1_dpd_timeout_seconds
  tunnel2_dpd_timeout_seconds = var.tunnel2_dpd_timeout_seconds

  tunnel1_dpd_timeout_action = var.tunnel1_dpd_timeout_action
  tunnel2_dpd_timeout_action = var.tunnel2_dpd_timeout_action

  tunnel1_phase2_dh_group_numbers = var.tunnel1_phase2_dh_group_numbers
  tunnel2_phase2_dh_group_numbers = var.tunnel2_phase2_dh_group_numbers

  tunnel1_phase2_encryption_algorithms = var.tunnel1_phase2_encryption_algorithms
  tunnel2_phase2_encryption_algorithms = var.tunnel2_phase2_encryption_algorithms

  tunnel1_phase2_integrity_algorithms = var.tunnel1_phase2_integrity_algorithms
  tunnel2_phase2_integrity_algorithms = var.tunnel2_phase2_integrity_algorithms

  tunnel1_phase2_lifetime_seconds = var.tunnel1_phase2_lifetime_seconds
  tunnel2_phase2_lifetime_seconds = var.tunnel2_phase2_lifetime_seconds

  tunnel1_rekey_fuzz_percentage = var.tunnel1_rekey_fuzz_percentage
  tunnel2_rekey_fuzz_percentage = var.tunnel2_rekey_fuzz_percentage

  tunnel1_rekey_margin_time_seconds = var.tunnel1_rekey_margin_time_seconds
  tunnel2_rekey_margin_time_seconds = var.tunnel2_rekey_margin_time_seconds

  tunnel1_replay_window_size = var.tunnel1_replay_window_size
  tunnel2_replay_window_size = var.tunnel2_replay_window_size

  tunnel1_startup_action = var.tunnel1_startup_action
  tunnel2_startup_action = var.tunnel2_startup_action

  tunnel1_ike_versions = var.tunnel1_ike_versions
  tunnel2_ike_versions = var.tunnel2_ike_versions

  tags = merge(var.tags, local.tags, {
    Name = lower("vpn-conn-${lower(var.name)}-tflz")
  })
}

data "aws_ec2_transit_gateway_vpn_attachment" "this" {
  transit_gateway_id = var.transit_gateway_id
  vpn_connection_id  = aws_vpn_connection.this.id

  depends_on = [aws_vpn_connection.this]
}

resource "aws_ec2_transit_gateway_route_table" "this" {
  transit_gateway_id = var.transit_gateway_id

  tags = merge(var.tags, local.tags, {
    Name = lower("tgw-rtb-vpn-tflz")
  })
}

resource "aws_ec2_transit_gateway_route_table_association" "this" {
  transit_gateway_attachment_id  = data.aws_ec2_transit_gateway_vpn_attachment.this.id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.this.id
}

locals {
  tgw_propagate_to_rtbs_map = { for propagation in var.tgw_propagate_to_rtbs : md5(propagation) => propagation }
  tgw_routes_map            = { for route in var.tgw_routes : md5("${route["cidr"]}_${route["attachement_id"]}") => route }
}

//resource "aws_ec2_transit_gateway_route" "tgw_routes" {
//  for_each = local.tgw_routes_map : {}
//
//  destination_cidr_block         = each.value["cidr"]
//  transit_gateway_attachment_id  = each.value["attachement_id"]
//  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.this.id
//}