# Terraform Module for AWS VPN

This module will create the required AWS resources to establish an S2S VPN connection between AWS  
and your local infrastructure. You will be able to use static or BGP based routing in conjunction  
with an existing AWS Transit Gateway for connection other VPCs attached to the AWS Transit Gateway.

Its part of a multi-stage network concept and designed to work with terraform workspaces.  
For a complete network setup, you have to include additional modules.

- VPC : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-vpc.git
- Transit Gateway : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-transit-gateway.git
- VPN : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-vpn.git
- Route53 : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-route53.git
- Direct Connect : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-direct-connect.git

***CAVE***: It's strongly required to name the terraform workspace & the shared vpc: ***shared***  
This is currently a hard coded value in all modules.

## Contributing
This module is intended to be a shared module.  
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Features
### Connection
- VPN connection with 2 active tunnels to a single customer gateway
- Single VPC attachment or TGW attachment

### Routing
- Static routes or BGP

## Examples

### VPN attached to TGW

    module "euc1_vpn" {

        source = "git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-vpn.git?ref=master"
        
        providers = {
            aws = aws.eu-central-1
        }
        
        name      = local.general.name
        workspace = terraform.workspace
        
        vpc_id = module.euc1_vpc.vpc_id
        
        vpn_attachment_type         = "TGW" 
        transit_gateway_id          = module.euc1_transit_gateway.transit_gateway_id
        vpn_routing_cidrs           = local.vpn_cidrs.euc1.datacenter
        bgp_asn                     = local.vpn_settings.euc1.bgp_asn
        enable_static_routing       = local.vpn_settings.euc1.enable_static_routing
        customer_gateway_ip_address = local.vpn_settings.euc1.customer_gateway_ip_address
        tunnel1_preshared_key       = local.vpn_settings.euc1.tunnel1_preshared_key
        tunnel2_preshared_key       = local.vpn_settings.euc1.tunnel2_preshared_key
        
        tags = local.common_tags
    }

### VPN attached to VPC

    module "euc1_vpn" {
    source = "git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-vpn.git?ref=master"
    
    providers = {
        aws = aws.eu-central-1
    }

    name      = local.general.name
    workspace = terraform.workspace
    
    vpc_id = module.euc1_vpc.vpc_id
    
    vpn_attachment_type         = "VPC"
    vpn_routing_cidrs           = local.vpn_cidrs.datacenter
    enable_static_routing       = local.vpn_settings.enable_static_routing
    customer_gateway_ip_address = local.vpn_settings.customer_gateway_ip_address
    tunnel1_preshared_key       = local.vpn_settings.tunnel1_preshared_key
    tunnel2_preshared_key       = local.vpn_settings.tunnel2_preshared_key
    
    tags = local.common_tags
    }


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13, < 0.15 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 2.68, < 4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 2.68, < 4.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_customer_gateway.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/customer_gateway) | resource |
| [aws_ssm_parameter.aws_vpn_gateway_id](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.transit_gateway_attachment_id](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_vpn_connection.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpn_connection) | resource |
| [aws_vpn_connection_route.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpn_connection_route) | resource |
| [aws_vpn_gateway.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpn_gateway) | resource |
| [aws_vpn_gateway_attachment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpn_gateway_attachment) | resource |
| [aws_ssm_parameter.transit_gateway_attachment_id](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ssm_parameter) | data source |
| [aws_ssm_parameter.vpn_gateway_id](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ssm_parameter) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_bgp_asn"></a> [bgp\_asn](#input\_bgp\_asn) | The Autonomous System Number (ASN) of the Amazon side of the gateway | `number` | `65000` | no |
| <a name="input_connect_to_transit_gateway"></a> [connect\_to\_transit\_gateway](#input\_connect\_to\_transit\_gateway) | Set to false to disable attachment of the VPN connection route to the VPN connection (TGW uses another resource for that) | `bool` | `false` | no |
| <a name="input_create_vpn_gateway_attachment"></a> [create\_vpn\_gateway\_attachment](#input\_create\_vpn\_gateway\_attachment) | Set to false to prevent attachment of the VGW to the VPC | `bool` | `true` | no |
| <a name="input_customer_gateway_ip_address"></a> [customer\_gateway\_ip\_address](#input\_customer\_gateway\_ip\_address) | External IP address of the Customer Gateway | `string` | `""` | no |
| <a name="input_enable_static_routing"></a> [enable\_static\_routing](#input\_enable\_static\_routing) | Whether the VPN connection uses static routes exclusively | `bool` | `false` | no |
| <a name="input_name"></a> [name](#input\_name) | Name to be used on all the resources as identifier | `string` | `""` | no |
| <a name="input_ssm_parameter"></a> [ssm\_parameter](#input\_ssm\_parameter) | Store the Transit Gateway Attachment ID in the SSM Parameter Store | `bool` | `true` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | A map of tags to add to all resources | `map(string)` | `{}` | no |
| <a name="input_transit_gateway_id"></a> [transit\_gateway\_id](#input\_transit\_gateway\_id) | The ID of the Transit Gateway | `string` | `""` | no |
| <a name="input_tunnel1_dpd_timeout_action"></a> [tunnel1\_dpd\_timeout\_action](#input\_tunnel1\_dpd\_timeout\_action) | (Optional, Default clear) The action to take after DPD timeout occurs for the first VPN tunnel. Specify restart to restart the IKE initiation. Specify clear to end the IKE session. Valid values are clear \| none \| restart | `string` | `null` | no |
| <a name="input_tunnel1_dpd_timeout_seconds"></a> [tunnel1\_dpd\_timeout\_seconds](#input\_tunnel1\_dpd\_timeout\_seconds) | (Optional, Default 30) The number of seconds after which a DPD timeout occurs for the first VPN tunnel. Valid value is equal or higher than 30 | `number` | `null` | no |
| <a name="input_tunnel1_ike_versions"></a> [tunnel1\_ike\_versions](#input\_tunnel1\_ike\_versions) | (Optional) The IKE versions that are permitted for the first VPN tunnel. Valid values are ikev1 \| ikev2 | `list(string)` | `null` | no |
| <a name="input_tunnel1_inside_cidr"></a> [tunnel1\_inside\_cidr](#input\_tunnel1\_inside\_cidr) | The CIDR block of the inside IP addresses for the first VPN tunnel. | `string` | `""` | no |
| <a name="input_tunnel1_phase1_dh_group_numbers"></a> [tunnel1\_phase1\_dh\_group\_numbers](#input\_tunnel1\_phase1\_dh\_group\_numbers) | (Optional) List of one or more Diffie-Hellman group numbers that are permitted for the first VPN tunnel for phase 1 IKE negotiations. Valid values are 2 \| 14 \| 15 \| 16 \| 17 \| 18 \| 19 \| 20 \| 21 \| 22 \| 23 \| 24 | `list(number)` | `null` | no |
| <a name="input_tunnel1_phase1_encryption_algorithms"></a> [tunnel1\_phase1\_encryption\_algorithms](#input\_tunnel1\_phase1\_encryption\_algorithms) | (Optional) List of one or more encryption algorithms that are permitted for the first VPN tunnel for phase 1 IKE negotiations. Valid values are AES128 \| AES256 \| AES128-GCM-16 \| AES256-GCM-16 | `list(string)` | `null` | no |
| <a name="input_tunnel1_phase1_integrity_algorithms"></a> [tunnel1\_phase1\_integrity\_algorithms](#input\_tunnel1\_phase1\_integrity\_algorithms) | (Optional) One or more integrity algorithms that are permitted for the first VPN tunnel for phase 1 IKE negotiations. Valid values are SHA1 \| SHA2-256 \| SHA2-384 \| SHA2-512 | `list(string)` | `null` | no |
| <a name="input_tunnel1_phase1_lifetime_seconds"></a> [tunnel1\_phase1\_lifetime\_seconds](#input\_tunnel1\_phase1\_lifetime\_seconds) | (Optional, Default 28800) The lifetime for phase 1 of the IKE negotiation for the first VPN tunnel, in seconds. Valid value is between 900 and 28800 | `number` | `null` | no |
| <a name="input_tunnel1_phase2_dh_group_numbers"></a> [tunnel1\_phase2\_dh\_group\_numbers](#input\_tunnel1\_phase2\_dh\_group\_numbers) | (Optional) List of one or more Diffie-Hellman group numbers that are permitted for the first VPN tunnel for phase 2 IKE negotiations. Valid values are 2 \| 5 \| 14 \| 15 \| 16 \| 17 \| 18 \| 19 \| 20 \| 21 \| 22 \| 23 \| 24 | `list(number)` | `null` | no |
| <a name="input_tunnel1_phase2_encryption_algorithms"></a> [tunnel1\_phase2\_encryption\_algorithms](#input\_tunnel1\_phase2\_encryption\_algorithms) | (Optional) List of one or more encryption algorithms that are permitted for the first VPN tunnel for phase 2 IKE negotiations. Valid values are AES128 \| AES256 \| AES128-GCM-16 \| AES256-GCM-16 | `list(string)` | `null` | no |
| <a name="input_tunnel1_phase2_integrity_algorithms"></a> [tunnel1\_phase2\_integrity\_algorithms](#input\_tunnel1\_phase2\_integrity\_algorithms) | (Optional) List of one or more integrity algorithms that are permitted for the first VPN tunnel for phase 2 IKE negotiations. Valid values are SHA1 \| SHA2-256 \| SHA2-384 \| SHA2-512 | `list(string)` | `null` | no |
| <a name="input_tunnel1_phase2_lifetime_seconds"></a> [tunnel1\_phase2\_lifetime\_seconds](#input\_tunnel1\_phase2\_lifetime\_seconds) | (Optional, Default 3600) The lifetime for phase 2 of the IKE negotiation for the first VPN tunnel, in seconds. Valid value is between 900 and 3600 | `number` | `null` | no |
| <a name="input_tunnel1_preshared_key"></a> [tunnel1\_preshared\_key](#input\_tunnel1\_preshared\_key) | The preshared key of the first VPN tunnel | `string` | `""` | no |
| <a name="input_tunnel1_rekey_fuzz_percentage"></a> [tunnel1\_rekey\_fuzz\_percentage](#input\_tunnel1\_rekey\_fuzz\_percentage) | (Optional, Default 100) The percentage of the rekey window for the first VPN tunnel (determined by tunnel1\_rekey\_margin\_time\_seconds) during which the rekey time is randomly selected. Valid value is between 0 and 100 | `number` | `null` | no |
| <a name="input_tunnel1_rekey_margin_time_seconds"></a> [tunnel1\_rekey\_margin\_time\_seconds](#input\_tunnel1\_rekey\_margin\_time\_seconds) | (Optional, Default 540) The margin time, in seconds, before the phase 2 lifetime expires, during which the AWS side of the first VPN connection performs an IKE rekey. The exact time of the rekey is randomly selected based on the value for tunnel1\_rekey\_fuzz\_percentage. Valid value is between 60 and half of tunnel1\_phase2\_lifetime\_seconds | `number` | `null` | no |
| <a name="input_tunnel1_replay_window_size"></a> [tunnel1\_replay\_window\_size](#input\_tunnel1\_replay\_window\_size) | (Optional, Default 1024) The number of packets in an IKE replay window for the first VPN tunnel. Valid value is between 64 and 2048. | `number` | `null` | no |
| <a name="input_tunnel1_startup_action"></a> [tunnel1\_startup\_action](#input\_tunnel1\_startup\_action) | (Optional, Default add) The action to take when the establishing the tunnel for the first VPN connection. By default, your customer gateway device must initiate the IKE negotiation and bring up the tunnel. Specify start for AWS to initiate the IKE negotiation. Valid values are add \| start | `string` | `null` | no |
| <a name="input_tunnel2_dpd_timeout_action"></a> [tunnel2\_dpd\_timeout\_action](#input\_tunnel2\_dpd\_timeout\_action) | (Optional, Default clear) The action to take after DPD timeout occurs for the second VPN tunnel. Specify restart to restart the IKE initiation. Specify clear to end the IKE session. Valid values are clear \| none \| restart | `string` | `null` | no |
| <a name="input_tunnel2_dpd_timeout_seconds"></a> [tunnel2\_dpd\_timeout\_seconds](#input\_tunnel2\_dpd\_timeout\_seconds) | (Optional, Default 30) The number of seconds after which a DPD timeout occurs for the second VPN tunnel. Valid value is equal or higher than 30 | `number` | `null` | no |
| <a name="input_tunnel2_ike_versions"></a> [tunnel2\_ike\_versions](#input\_tunnel2\_ike\_versions) | (Optional) The IKE versions that are permitted for the first VPN tunnel. Valid values are ikev1 \| ikev2 | `list(string)` | `null` | no |
| <a name="input_tunnel2_inside_cidr"></a> [tunnel2\_inside\_cidr](#input\_tunnel2\_inside\_cidr) | The CIDR block of the inside IP addresses for the second VPN tunnel. | `string` | `""` | no |
| <a name="input_tunnel2_phase1_dh_group_numbers"></a> [tunnel2\_phase1\_dh\_group\_numbers](#input\_tunnel2\_phase1\_dh\_group\_numbers) | (Optional) List of one or more Diffie-Hellman group numbers that are permitted for the second VPN tunnel for phase 1 IKE negotiations. Valid values are 2 \| 14 \| 15 \| 16 \| 17 \| 18 \| 19 \| 20 \| 21 \| 22 \| 23 \| 24 | `list(number)` | `null` | no |
| <a name="input_tunnel2_phase1_encryption_algorithms"></a> [tunnel2\_phase1\_encryption\_algorithms](#input\_tunnel2\_phase1\_encryption\_algorithms) | (Optional) List of one or more encryption algorithms that are permitted for the second VPN tunnel for phase 1 IKE negotiations. Valid values are AES128 \| AES256 \| AES128-GCM-16 \| AES256-GCM-16 | `list(string)` | `null` | no |
| <a name="input_tunnel2_phase1_integrity_algorithms"></a> [tunnel2\_phase1\_integrity\_algorithms](#input\_tunnel2\_phase1\_integrity\_algorithms) | (Optional) One or more integrity algorithms that are permitted for the second VPN tunnel for phase 1 IKE negotiations. Valid values are SHA1 \| SHA2-256 \| SHA2-384 \| SHA2-512 | `list(string)` | `null` | no |
| <a name="input_tunnel2_phase1_lifetime_seconds"></a> [tunnel2\_phase1\_lifetime\_seconds](#input\_tunnel2\_phase1\_lifetime\_seconds) | (Optional, Default 28800) The lifetime for phase 1 of the IKE negotiation for the second VPN tunnel, in seconds. Valid value is between 900 and 28800 | `number` | `null` | no |
| <a name="input_tunnel2_phase2_dh_group_numbers"></a> [tunnel2\_phase2\_dh\_group\_numbers](#input\_tunnel2\_phase2\_dh\_group\_numbers) | (Optional) List of one or more Diffie-Hellman group numbers that are permitted for the second VPN tunnel for phase 2 IKE negotiations. Valid values are 2 \| 5 \| 14 \| 15 \| 16 \| 17 \| 18 \| 19 \| 20 \| 21 \| 22 \| 23 \| 24 | `list(number)` | `null` | no |
| <a name="input_tunnel2_phase2_encryption_algorithms"></a> [tunnel2\_phase2\_encryption\_algorithms](#input\_tunnel2\_phase2\_encryption\_algorithms) | (Optional) List of one or more encryption algorithms that are permitted for the second VPN tunnel for phase 2 IKE negotiations. Valid values are AES128 \| AES256 \| AES128-GCM-16 \| AES256-GCM-16 | `list(string)` | `null` | no |
| <a name="input_tunnel2_phase2_integrity_algorithms"></a> [tunnel2\_phase2\_integrity\_algorithms](#input\_tunnel2\_phase2\_integrity\_algorithms) | (Optional) List of one or more integrity algorithms that are permitted for the second VPN tunnel for phase 2 IKE negotiations. Valid values are SHA1 \| SHA2-256 \| SHA2-384 \| SHA2-512 | `list(string)` | `null` | no |
| <a name="input_tunnel2_phase2_lifetime_seconds"></a> [tunnel2\_phase2\_lifetime\_seconds](#input\_tunnel2\_phase2\_lifetime\_seconds) | (Optional, Default 3600) The lifetime for phase 2 of the IKE negotiation for the second VPN tunnel, in seconds. Valid value is between 900 and 3600 | `number` | `null` | no |
| <a name="input_tunnel2_preshared_key"></a> [tunnel2\_preshared\_key](#input\_tunnel2\_preshared\_key) | The preshared key of the second VPN tunnel | `string` | `""` | no |
| <a name="input_tunnel2_rekey_fuzz_percentage"></a> [tunnel2\_rekey\_fuzz\_percentage](#input\_tunnel2\_rekey\_fuzz\_percentage) | (Optional, Default 100) The percentage of the rekey window for the second VPN tunnel (determined by tunnel1\_rekey\_margin\_time\_seconds) during which the rekey time is randomly selected. Valid value is between 0 and 100 | `number` | `null` | no |
| <a name="input_tunnel2_rekey_margin_time_seconds"></a> [tunnel2\_rekey\_margin\_time\_seconds](#input\_tunnel2\_rekey\_margin\_time\_seconds) | (Optional, Default 540) The margin time, in seconds, before the phase 2 lifetime expires, during which the AWS side of the second VPN connection performs an IKE rekey. The exact time of the rekey is randomly selected based on the value for tunnel2\_rekey\_fuzz\_percentage. Valid value is between 60 and half of tunnel2\_phase2\_lifetime\_seconds | `number` | `null` | no |
| <a name="input_tunnel2_replay_window_size"></a> [tunnel2\_replay\_window\_size](#input\_tunnel2\_replay\_window\_size) | (Optional, Default 1024) The number of packets in an IKE replay window for the second VPN tunnel. Valid value is between 64 and 2048. | `number` | `null` | no |
| <a name="input_tunnel2_startup_action"></a> [tunnel2\_startup\_action](#input\_tunnel2\_startup\_action) | (Optional, Default add) The action to take when the establishing the tunnel for the second VPN connection. By default, your customer gateway device must initiate the IKE negotiation and bring up the tunnel. Specify start for AWS to initiate the IKE negotiation. Valid values are add \| start | `string` | `null` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | ID of the VPC | `string` | `""` | no |
| <a name="input_vpn_attachment_type"></a> [vpn\_attachment\_type](#input\_vpn\_attachment\_type) | Defines the VPN attachment type (VPC or TGW) Case sensitive !! | `string` | `"TGW"` | no |
| <a name="input_vpn_routing_cidrs"></a> [vpn\_routing\_cidrs](#input\_vpn\_routing\_cidrs) | The CIDR blocks associated with the local subnet of the customer network | `list(string)` | `[]` | no |
| <a name="input_workspace"></a> [workspace](#input\_workspace) | Name to be used on all the resources as identifier for the Terraform workspace | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aws_vpn_gateway_id"></a> [aws\_vpn\_gateway\_id](#output\_aws\_vpn\_gateway\_id) | n/a |
| <a name="output_transit_gateway_attachment_id"></a> [transit\_gateway\_attachment\_id](#output\_transit\_gateway\_attachment\_id) | n/a |
| <a name="output_tunnel1_bgp_asn"></a> [tunnel1\_bgp\_asn](#output\_tunnel1\_bgp\_asn) | n/a |
| <a name="output_tunnel1_cgw_inside_address"></a> [tunnel1\_cgw\_inside\_address](#output\_tunnel1\_cgw\_inside\_address) | n/a |
| <a name="output_tunnel1_vgw_inside_address"></a> [tunnel1\_vgw\_inside\_address](#output\_tunnel1\_vgw\_inside\_address) | n/a |
| <a name="output_tunnel2_bgp_asn"></a> [tunnel2\_bgp\_asn](#output\_tunnel2\_bgp\_asn) | n/a |
| <a name="output_tunnel2_cgw_inside_address"></a> [tunnel2\_cgw\_inside\_address](#output\_tunnel2\_cgw\_inside\_address) | n/a |
| <a name="output_tunnel2_vgw_inside_address"></a> [tunnel2\_vgw\_inside\_address](#output\_tunnel2\_vgw\_inside\_address) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
